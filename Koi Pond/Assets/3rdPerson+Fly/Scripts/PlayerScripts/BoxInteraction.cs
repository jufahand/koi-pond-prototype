using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxInteraction : MonoBehaviour
{

    public GameObject player;
    public Animator animator;
    public GameObject text;
    

    private int i_open;
    private bool m_atBox;

    // Start is called before the first frame update
    void Start()
    {
        text.SetActive(false);
        m_atBox = false;
        i_open = 0;
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == player && i_open == 0)
        {
            m_atBox = true;
            text.SetActive(true);
        }
    }

    void OnTriggerExit (Collider other)
    {
        if (other.gameObject == player)
        {
            m_atBox = false;
            text.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_atBox && i_open == 0)
        {
            Debug.Log("At Box");
            text.SetActive(true);
        }
        else
        {
            Debug.Log("Away");
            text.SetActive(false);
        }

        if (m_atBox && Input.GetButton("Jump") && i_open == 0)
        {
                animator.Play("Box Open");
                i_open = i_open + 1;
        }
    }
}